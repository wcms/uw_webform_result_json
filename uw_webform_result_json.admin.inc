<?php

/**
 * @file
 * Administrative pages.
 */

/**
 * The administrative page.
 */
function uw_webform_result_json_admin() {
  $render = array();
  $render['box'] = array(
    '#type' => 'fieldset',
  );
  $render['box']['message'] = array(
    '#type' => 'markup',
    '#markup' => 'The current security key is: ' . variable_get('uw_webform_result_json_key'),
  );
  $render['box']['reset'] = drupal_get_form('uw_webform_result_json_reset_key_form');
  return $render;
}

/**
 * Form constructor to reset the security key.
 *
 * @see uw_webform_result_json_reset_key_form_submit()
 */
function uw_webform_result_json_reset_key_form($form, &$form_state) {
  return array('reset' => array('#type' => 'submit', '#value' => 'Reset Key'));
}

/**
 * Form submission for watiam_reset_key_form()
 *
 * @see uw_webform_result_json_reset_key_form_submit()
 */
function uw_webform_result_json_reset_key_form_submit($form, &$form_state) {
  variable_set('uw_webform_result_json_key', md5(rand()));
}
