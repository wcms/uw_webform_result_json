<?php

/**
 * @file
 * uw_webform_result_json.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_webform_result_json_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
